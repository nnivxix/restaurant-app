const CONFIG = {
	BASE_URL: 'https://restaurant-api.dicoding.dev/',
	BASE_IMAGE_URL_MD: 'https://restaurant-api.dicoding.dev/images/small/',
	BASE_IMAGE_URL_SM: 'https://restaurant-api.dicoding.dev/images/medium/',
	DEFAULT_LANGUAGE: 'en-us',
	CACHE_NAME: `Foodlive__${new Date().toISOString()}`,
	DATABASE_NAME: 'foodlive-database',
	DATABASE_VERSION: 1,
	OBJECT_STORE_NAME: 'restaurant',
};

export default CONFIG;
