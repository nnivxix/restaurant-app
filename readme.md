# Restaurant App


## Submission 1
- [x] Navbar
- [x] Hero
- [x] Cards
- [x] Footer
- [x] Skip to Content
- [x] Favicon
- [x] Rendering cards
- [x] Functional with the keyboard
- [x] ***build*** ok

### Saran Submission 1
- [x] Pada tag `<a>` tambahkan atribut rel="noopener" atau rel="noreferrer". Hal ini bertujuan dapat improve performa website
- [x] Sebaiknya element image restoran yang kamu tampilkan pada halaman home diberi ukuran(width dan height) yang sama agar lebih rapi dan seragam.

	```css
		width: 100%;
		height: 270px;
		object-fit: cover;
		object-position: center;
	```
- [] Memanfaatkan plugin `favicons-webpack-plugin` untuk meload favicon.
- [x] Tambahkan meta tag Description pada berkas html.
- [x] Cobalah membuat Web Components pada beberapa komponen UI yang bersifat Reusable.
- [x] Perhatikan _style guide_ dan gunakan ***Linter***



## Submission 2
### Feature
- [] Our Story
- [] Subscribe News Letter (promo)
- [x] Fetch Data
- [x] Restructure AppBar
- [x] Buat Halaman `[Home, detail, fav]` => malam tgl 1
- [x] Buat komponen `hero-app` dan `card` => malam tgl 1
- [x] Config esLint 
- [x] using _ / underscore character

### Req
- [x] ESLint => tgl 1
- [x] Appshell
- [x] `manifest.json`
- [x] Service Worker  => tgl 1
- [x] Routes
- [x] Cache
- [x] IndexedDB
- ~[ ] WebSocket dan Notification~